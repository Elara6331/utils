module go.arsenm.dev/utils

go 1.17

require github.com/gin-gonic/gin v1.7.7

require (
	github.com/golang/protobuf v1.3.3 // indirect
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742 // indirect
	github.com/ugorji/go/codec v1.1.7 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
